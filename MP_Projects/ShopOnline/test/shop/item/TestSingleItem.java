package shop.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TestSingleItem {

	SingleItem item = new SingleItem("name", 5.00);

	@Test
	public void testGetPrice() {
		assertNotEquals(6.00, item.getPrice(), 0.01);
		assertEquals(5.00, item.getPrice(), 0.01);
	}

	@Test
	public void testSetPrice() {
		item.setPrice(6.00);
		assertNotEquals(5.00, item.getPrice(), 0.01);
		assertEquals(6.00, item.getPrice(), 0.01);
		item.setPrice(5.00);
		assertEquals(5.00, item.getPrice(), 0.01);
	}

	@Test
	public void testToString() {
		assertEquals("Name : name Price : " + item.getPrice() + "\n", item.toString());
		assertNotEquals("name", item.getPrice());
	}

	@Test
	public void testGetName() {
		assertNotEquals("namee", item.getName());
		assertEquals("name", item.getName());
	}

	@Test
	public void testSetName() {
		item.setName("Name");
		assertNotEquals("name", item.getName());
		assertEquals("Name", item.getName());
		item.setName("name");
		assertEquals("name", item.getName());
	}
}
