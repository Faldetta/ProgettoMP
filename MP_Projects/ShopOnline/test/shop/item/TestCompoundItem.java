package shop.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class TestCompoundItem {

	private CompoundItem compound;
	private SingleItem single;

	@Before
	public void init() {
		this.compound = new CompoundItem("nameCompound");
		this.single = new SingleItem("nameSingle", 5.00);
		compound.addItem(single);
	}

	@Test
	public void testAddItem() {
		LinkedList<Item> itemList = compound.getItemsList();
		assertEquals(itemList.getFirst(), itemList.getLast());
		assertEquals(itemList.getFirst(), single);
	}

	@Test
	public void testRemoveItem() {
		LinkedList<Item> itemList = compound.getItemsList();
		assertEquals(itemList.getFirst(), itemList.getLast());
		assertEquals(itemList.getFirst(), single);
		compound.removeItem(single);
		itemList = compound.getItemsList();
		assertFalse(itemList.remove(single));
	}

	@Test
	public void testGetPrice() {
		assertNotEquals(6.00, compound.getPrice(), 0.01);
		assertEquals(5.00, compound.getPrice(), 0.01);
	}

	@Test
	public void testToString() {
		assertEquals("Name : nameCompound\nPrice : 5.0\nDescription : \n   1. Name : nameSingle Price : 5.0\n\n\n",
				compound.toString());
	}

	@Test
	public void testGetItemList() {
		assertEquals(single, compound.getItemsList().getFirst());
	}

	@Test
	public void testGetName() {
		assertEquals("nameCompound", compound.getName());
	}

}
