package shop.cart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import shop.item.CompoundItem;
import shop.item.SingleItem;

public class TestCart {

	Cart cart;
	SingleItem single = new SingleItem("single01", 5.00);

	@Before
	public void setUp() {
		cart = new Cart();
		cart.deleteObservers();
	}

	@Test
	public void testAddItem() {
		cart.addItem(single);
		assertEquals(5.00, cart.getCart().getItemsList().getFirst().getPrice(), 0.01);
		assertEquals("single01", cart.getCart().getItemsList().getFirst().getName());
	}

	@Test
	public void testRemoveItem() {
		cart.addItem(single);
		assertEquals(5.00, cart.getCart().getItemsList().getFirst().getPrice(), 0.01);
		assertEquals("single01", cart.getCart().getItemsList().getFirst().getName());
		cart.removeItem(single);
		assertFalse(cart.getCart().getItemsList().remove(single));
	}

	@Test
	public void testToString() {
		CompoundItem compound = new CompoundItem("Cart");
		assertEquals(compound.toString(), cart.toString());
		compound.addItem(single);
		cart.addItem(single);
		assertEquals(compound.toString(), cart.toString());
	}

	@Test
	public void testTotal() {
		assertEquals(0.00, cart.total(), 0.01);
		cart.addItem(single);
		assertEquals(5.00, cart.total(), 0.01);
		cart.addItem(single);
		assertEquals(10.00, cart.total(), 0.01);
	}

	@Test
	public void testReset() {
		cart.addItem(single);
		assertEquals(5.00, cart.total(), 0.01);
		cart.reset();
		assertEquals(0.00, cart.total(), 0.01);
	}

}
