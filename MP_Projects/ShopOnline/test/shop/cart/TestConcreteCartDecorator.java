package shop.cart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestConcreteCartDecorator {

	CartInterface cart = new ConcreteCartDecoretor(new Cart());

	@Test
	public void testToString() {
		assertEquals("########## SHOP ONLINE ########\nName : Cart\nPrice : 0.0\nDescription : \n\n", cart.toString());
	}

}
