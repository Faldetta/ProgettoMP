package shop.report;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import shop.item.CompoundItem;
import shop.item.SingleItem;

public class TestVisitorMin {

	VisitorMin vM;
	CompoundItem compound;
	SingleItem min;

	@Before
	public void setUp() {
		vM = new VisitorMin();
		compound = new CompoundItem("compoundName");
		min = new SingleItem("single01", 2.00);
		compound.addItem(min);
		min = new SingleItem("single02", 1.00);
		compound.addItem(min);
	}

	@Test
	public void testVisitSingleItem() {
		min.accept(vM);
		assertEquals(min, vM.getItem());
	}

	@Test
	public void testVisitCompoundItem() {
		compound.accept(vM);
		assertEquals(min.getPrice(), vM.getItem().getPrice(), 0.01);
		assertEquals(min.getName(), vM.getItem().getName());
	}

}
