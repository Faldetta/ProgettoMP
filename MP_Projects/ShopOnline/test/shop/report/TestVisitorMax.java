package shop.report;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import shop.item.CompoundItem;
import shop.item.SingleItem;

public class TestVisitorMax {

	VisitorMax vM;
	CompoundItem compound;
	SingleItem max;

	@Before
	public void setUp() {
		vM = new VisitorMax();
		compound = new CompoundItem("compoundName");
		max = new SingleItem("single01", 1.00);
		compound.addItem(max);
		max = new SingleItem("single02", 2.00);
		compound.addItem(max);
	}

	@Test
	public void testVisitSingleItem() {
		max.accept(vM);
		assertEquals(max, vM.getItem());
	}

	@Test
	public void testVisitCompoundItem() {
		compound.accept(vM);
		assertEquals(max.getPrice(), vM.getItem().getPrice(), 0.01);
		assertEquals(max.getName(), vM.getItem().getName());
	}

}
