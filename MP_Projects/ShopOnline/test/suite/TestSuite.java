package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import shop.cart.TestCart;
import shop.cart.TestConcreteCartDecorator;
import shop.item.TestCompoundItem;
import shop.item.TestSingleItem;
import shop.report.TestVisitorMax;
import shop.report.TestVisitorMin;

@RunWith(Suite.class)
@Suite.SuiteClasses({ TestCart.class, TestConcreteCartDecorator.class, TestCompoundItem.class, TestSingleItem.class,
		TestVisitorMax.class, TestVisitorMin.class })
public class TestSuite {
}
