package shop.item;

import java.util.Iterator;
import java.util.LinkedList;

import shop.report.Visitor;

public class CompoundItem implements Item {

	private LinkedList<Item> itemsList;
	private double price;
	private String name;

	public CompoundItem(String name) {
		itemsList = new LinkedList<Item>();
		price = 0;
		this.name = new String(name);
	}

	@Override
	public void addItem(Item item) {
		itemsList.add(item);
		price += item.getPrice();

	}

	@Override
	public void removeItem(Item item) {
		if (itemsList.contains(item)) {
			itemsList.remove(item);
			price -= item.getPrice();
		}

	}

	@Override
	public double getPrice() {
		return price;
	}


	@Override
	public String toString() {
		String description = new String("Name : " + name + "\nPrice : " + price + "\nDescription : ");
		Iterator<Item> it = itemsList.iterator();
		int index = 1;
		while (it.hasNext()) {
			description += "\n" + "   " + index + ". " + it.next().toString();
			index++;
		}
		description += "\n\n";
		return description;
	}

	public LinkedList<Item> getItemsList() {
		return itemsList;
	}

	@Override
	public void accept(Visitor v) {
		v.visitCompoundItem(this);
	}

	@Override
	public String getName() {
		return name;
	}

}
