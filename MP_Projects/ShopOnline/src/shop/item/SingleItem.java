package shop.item;

import shop.report.Visitor;

public class SingleItem implements Item {
	private double price;
	private String name;

	public SingleItem(String name, double price) {
		this.price = price;
		this.name = name;
	}

	@Override
	public double getPrice() {
		return price;
	}

	public void setPrice(double newPrice) {
		price = newPrice;
	}

	@Override
	public String toString() {
		return new String("Name : " + name + " Price : " + price +"\n");
	}

	@Override
	public void addItem(Item item) {
	}

	@Override
	public void removeItem(Item item) {
	}

	@Override
	public void accept(Visitor v) {
		v.visitSignleItem(this);
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String newName) {
		name = newName;
	}



}
