package shop.item;

import shop.report.Visitor;

public interface Item {

	public double getPrice();
	
	public String getName();

	public String toString();

	public void addItem(Item item);

	public void removeItem(Item item);

	public void accept (Visitor visitor);

}
