package shop.engine;

import shop.cart.Cart;
import shop.cart.ConcreteCartDecoretor;
import shop.gui.MainFrame;

public class Main {

	public static void main(String[] args) {
		Main engine = new Main();
		engine.run();
	}

	private void run() {
		new MainFrame(new ConcreteCartDecoretor(new Cart()));
	}
}
