package shop.cart;

import shop.item.CompoundItem;
import shop.item.Item;
import shop.item.SingleItem;

public interface CartInterface {
	
	void addItem(Item item);

	public void removeItem(Item item);

	public String toString();

	public double total();

	public CompoundItem getCart();

	public void reset();

	public SingleItem max();

	public SingleItem min();

}
