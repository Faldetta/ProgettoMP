package shop.cart;

import java.util.Observable;

import shop.item.CompoundItem;
import shop.item.Item;
import shop.item.SingleItem;
import shop.report.VisitorMax;
import shop.report.VisitorMin;

public class Cart extends Observable implements CartInterface {

	private CompoundItem cart;

	public Cart() {
		super();
		cart = new CompoundItem("Cart");
		addObserver(new SideCart());
	}

	public void addItem(Item item) {
		cart.addItem(item);
		setChanged();
		notifyObservers(this);
	}

	public void removeItem(Item item) {
		cart.removeItem(item);
		setChanged();
		notifyObservers(this);
	}

	public String toString() {
		return cart.toString();
	}

	public double total() {
		return cart.getPrice();
	}

	public CompoundItem getCart() {
		return cart;
	}

	public void reset() {
		cart = new CompoundItem("Cart");
		setChanged();
		notifyObservers(this);
	}

	public SingleItem max() {
		VisitorMax vMax = new VisitorMax();
		cart.accept(vMax);
		return vMax.getItem();
	}

	public SingleItem min() {
		VisitorMin vMin = new VisitorMin();
		cart.accept(vMin);
		return vMin.getItem();
	}

}
