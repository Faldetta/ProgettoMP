package shop.cart;

import shop.item.CompoundItem;
import shop.item.Item;
import shop.item.SingleItem;

public abstract class AbstractCardDecorator implements CartInterface {

	private Cart cart = new Cart();

	public AbstractCardDecorator(Cart cart) {
		this.cart = cart;
	}
	
	public String toString(){
		String rst;
		if(cart!=null){
			rst=cart.toString();
		} else{
			rst=null;
		}
		return rst;
	}

	@Override
	public void addItem(Item item) {
		cart.addItem(item);

	}

	@Override
	public void removeItem(Item item) {
		cart.removeItem(item);
	}

	@Override
	public double total() {
		return cart.total();
	}

	@Override
	public CompoundItem getCart() {
		return cart.getCart();
	}

	@Override
	public void reset() {
		cart.reset();
	}

	@Override
	public SingleItem max() {
		return cart.max();
	}

	@Override
	public SingleItem min() {
		return cart.min();
	}

}
