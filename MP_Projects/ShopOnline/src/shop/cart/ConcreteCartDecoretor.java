package shop.cart;

public class ConcreteCartDecoretor extends AbstractCardDecorator {

	public ConcreteCartDecoretor(Cart cart) {
		super(cart);
	}

	public String toString() {
		String rst = headerCart() + super.toString();
		return rst;
	}

	public String headerCart() {
		return "########## SHOP ONLINE ########\n";
	}
}
