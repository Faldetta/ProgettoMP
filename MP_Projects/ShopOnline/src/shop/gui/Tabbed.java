package shop.gui;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import shop.cart.CartInterface;
import shop.item.CompoundItem;
import shop.item.SingleItem;

@SuppressWarnings("serial")
public class Tabbed extends JTabbedPane {

	private CartInterface cart;
	private JPanel panelSinItem = new JPanel();
	private JPanel panelCompItem = new JPanel();

	public Tabbed(CartInterface cart) {
		super();
		this.cart = cart;
		setTab();
		addPanel();
		addButton();
	}

	public void setTab() {
		panelSinItem.setSize(450, 500);
	}

	public void addPanel() {
		addTab("First Category", panelSinItem);
		addTab("Second Category", panelCompItem);
	}

	private void addButton() {
		SingleItem itemOne = new SingleItem("Item 1",73);
		JButton bottonItemOne = new JButton("Item 1");
		bottonItemOne.addMouseListener(new MouseListener(itemOne, cart));
		panelSinItem.add(bottonItemOne);
		SingleItem itemTwo = new SingleItem("Item 2",25);
		JButton bottonItemTwo = new JButton("Item 2");
		bottonItemTwo.addMouseListener(new MouseListener(itemTwo, cart));
		panelSinItem.add(bottonItemTwo);
		SingleItem itemThree = new SingleItem("Item 3",50.5);
		JButton bottonItemThree = new JButton("Item 3");
		bottonItemThree.addMouseListener(new MouseListener(itemThree, cart));
		panelSinItem.add(bottonItemThree);
		SingleItem itemFour = new SingleItem("Item 4",73);
		JButton bottonItemFour = new JButton("Item 4");
		bottonItemFour.addMouseListener(new MouseListener(itemFour, cart));
		panelSinItem.add(bottonItemFour);
		
		CompoundItem compoundOne= new CompoundItem("Compound 1");
		JButton itemCompOne = new JButton("Compound 1");
		itemCompOne.addMouseListener(new MouseListener(compoundOne, cart));
		compoundOne.addItem(itemOne);
		compoundOne.addItem(itemOne);
		compoundOne.addItem(itemOne);
		compoundOne.addItem(itemOne);
		panelCompItem.add(itemCompOne);
		CompoundItem compoundTwo= new CompoundItem("Compound 2");
		JButton itemCompTwo = new JButton("Compound 2");
		itemCompTwo.addMouseListener(new MouseListener(compoundTwo, cart));
		compoundTwo.addItem(itemTwo);
		compoundTwo.addItem(itemTwo);
		compoundTwo.addItem(itemTwo);
		compoundTwo.addItem(itemTwo);
		panelCompItem.add(itemCompTwo);
		CompoundItem compoundThree= new CompoundItem("Compound 3");
		JButton itemCompThree = new JButton("Compound 3");
		itemCompThree.addMouseListener(new MouseListener(compoundThree, cart));
		compoundThree.addItem(itemThree);
		compoundThree.addItem(itemThree);
		compoundThree.addItem(itemThree);
		compoundThree.addItem(itemThree);
		panelCompItem.add(itemCompThree);
		CompoundItem compoundFour= new CompoundItem("Compound 4");
		JButton itemCompFour = new JButton("Compound 4");
		itemCompFour.addMouseListener(new MouseListener(compoundFour, cart));
		compoundFour.addItem(itemFour);
		compoundFour.addItem(itemFour);
		compoundFour.addItem(itemFour);
		compoundFour.addItem(itemFour);
		panelCompItem.add(itemCompFour);

		
	}

}
