package shop.gui;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import shop.cart.CartInterface;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private CartInterface cart;

	public MainFrame(CartInterface cart) {
		super("Shop");
		this.cart = cart;
		setFrame();
		add(new Tabbed(this.cart), BorderLayout.CENTER);
		setPanels();
		formatting();
	}

	private void setPanels() {
		JPanel panel = new JPanel();
		JPanel buttonPanel = new JPanel();
		panel.setLayout((new BoxLayout(panel, BoxLayout.Y_AXIS)));
		JButton report = new JButton("MAX / MIN");
		JButton bill = new JButton("PAGAMENTO / RESET");
		bill.addMouseListener(new BillListener(cart, this));
		report.addMouseListener(new ReportListener(cart));
		buttonPanel.add(report);
		buttonPanel.add(bill);
		panel.add(buttonPanel);
		panel.add(EditorPane.getEditor());
		add(panel, BorderLayout.EAST);
	}

	private void formatting() {
		pack();
		setSize(700, 500);
	}

	private void setFrame() {
		setVisible(true);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(100, 100);
		setLayout(new BorderLayout());
	}

}
