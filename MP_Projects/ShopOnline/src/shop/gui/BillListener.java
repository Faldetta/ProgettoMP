package shop.gui;

import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import shop.cart.CartInterface;
import shop.payment.PaymentStrategy;

public class BillListener extends MouseInputAdapter {

	private CartInterface cart;
	private JFrame frame;

	public BillListener(CartInterface cart, JFrame frame) {
		super();
		this.cart = cart;
		this.frame = frame;
	}

	public void mouseClicked(MouseEvent e) {
		String bill = "";
		if (SwingUtilities.isLeftMouseButton(e)) {
			bill = paymentPressed(bill);
		} else if (SwingUtilities.isRightMouseButton(e)) {
			cart.reset();
		}
		EditorPane.getEditor().setText(bill);
	}

	private String paymentPressed(String bill) {
		if (cart.total() != 0) {
			PaymentSelector selector = new PaymentSelector(frame, "Pagamento");
			PaymentStrategy strategy = selector.paymentMethod();
			strategy.pay(cart);
		} else {
			bill = "CARRELLO VUOTO";
		}
		return bill;
	}

}
