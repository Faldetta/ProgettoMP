package shop.gui;

import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import shop.payment.Bitcoin;
import shop.payment.CreditCard;
import shop.payment.PayPal;
import shop.payment.PaymentStrategy;

@SuppressWarnings("serial")
public class PaymentSelector extends JDialog {

	private static String[] options = { "PayPal", "Credit Card", "Bitcoin" };
	private static Object result;

	public PaymentSelector(Frame owner, String title) {
		super(owner, title, true);
	}

	public PaymentStrategy paymentMethod() {
		result = JOptionPane.showInputDialog(null, "scegliere metodo di pagamento", "Pagamento",
				JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
		PaymentStrategy strategy = null;
		switch (result.toString()) {
		case ("PayPal"):
			strategy = new PayPal();
			break;
		case ("Credit Card"):
			strategy = new CreditCard();
			break;
		case ("Bitcoin"):
			strategy = new Bitcoin();
			break;
		}
		return strategy;
	}

}
