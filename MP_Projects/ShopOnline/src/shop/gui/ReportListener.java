package shop.gui;

import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import shop.cart.CartInterface;
import shop.item.SingleItem;

public class ReportListener extends MouseInputAdapter {

	private CartInterface cart;

	public ReportListener(CartInterface cart) {
		super();
		this.cart = cart;
	}

	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			if (cart.max() != null) {
				SingleItem max = cart.max();
				JOptionPane.showMessageDialog(null, ("Massimo : " + max.toString()));
			}
		} else if (SwingUtilities.isRightMouseButton(e)) {
			if (cart.min() != null) {
				SingleItem min = cart.min();
				JOptionPane.showMessageDialog(null, ("Minimo : " + min.toString()));
			}
		}
	}

}
