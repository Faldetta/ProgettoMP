package shop.gui;

import java.awt.event.MouseEvent;


import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import shop.cart.CartInterface;
import shop.item.Item;

public class MouseListener extends MouseInputAdapter {

	private Item item;
	private CartInterface cart;

	public MouseListener(Item item, CartInterface cart) {
		super();
		this.item = item;
		this.cart = cart;
	}

	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			cart.addItem(item);
		} else if (SwingUtilities.isRightMouseButton(e)) {
			cart.removeItem(item);
		}
	}

}
