package shop.gui;

import java.awt.BorderLayout;

import javax.swing.JEditorPane;
import javax.swing.JFrame;

public class BillInterface {

	public BillInterface(String cartString, String strategy) {
		String testo = new String("FATTURA : \n	" + cartString + "\n	Tipo Pagamento : " + strategy
				+ " \nPagamento effettuato con successo");
		JFrame frame = new JFrame("Fattura");
		frame.setVisible(true);
		frame.setResizable(true);
		frame.setLocation(200, 200);
		frame.setLayout(new BorderLayout());
		JEditorPane editor = new JEditorPane("txt", testo);
		editor.setEditable(false);
		frame.add(editor, BorderLayout.CENTER);
		frame.pack();
	}

}
