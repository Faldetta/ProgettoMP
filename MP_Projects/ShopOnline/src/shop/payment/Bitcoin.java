package shop.payment;

import shop.cart.CartInterface;
import shop.gui.BillInterface;

public class Bitcoin implements PaymentStrategy {

	public void pay(CartInterface cart) {
		new BillInterface(cart.toString(), "Bitcoin");
		cart.reset();
	}

}
