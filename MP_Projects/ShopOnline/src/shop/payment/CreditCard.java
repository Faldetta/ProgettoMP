package shop.payment;

import shop.cart.CartInterface;
import shop.gui.BillInterface;

public class CreditCard implements PaymentStrategy {

	public void pay(CartInterface cart) {
		new BillInterface(cart.toString(), "Carta di Credito");
		cart.reset();
	}

}
