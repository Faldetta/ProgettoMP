package shop.payment;

import shop.cart.CartInterface;

public interface PaymentStrategy {

	public abstract void pay(CartInterface cart);

}
