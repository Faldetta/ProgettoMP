package shop.payment;

import shop.cart.CartInterface;
import shop.gui.BillInterface;

public class PayPal implements PaymentStrategy {

	public void pay(CartInterface cart) {
		new BillInterface(cart.toString(), "PayPal");
		cart.reset();
	}

}
