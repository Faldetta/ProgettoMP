package shop.report;

import java.util.Iterator;

import shop.item.CompoundItem;
import shop.item.Item;
import shop.item.SingleItem;

public class VisitorMin implements Visitor {

	private SingleItem champion;

	@Override
	public void visitSignleItem(SingleItem item) {
		if (champion==null){
			champion=item;
		}
		if (this.champion.getPrice() >= item.getPrice()) {
			swichItem(item);
		}
	}

	@Override
	public void visitCompoundItem(CompoundItem item) {
		Iterator<Item> it = item.getItemsList().iterator();
		while (it.hasNext()) {
			it.next().accept(this);
		}
	}

	private void swichItem(SingleItem item) {
		this.champion.setName(item.getName());
		this.champion.setPrice(item.getPrice());
	}

	public SingleItem getItem() {
		if (champion==null){
			return null;
		}
		return this.champion;
	}

}
