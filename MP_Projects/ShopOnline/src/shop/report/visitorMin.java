package shop.report;

import java.util.Iterator;

import shop.item.CompoundItem;
import shop.item.Item;
import shop.item.SingleItem;

public class visitorMin implements Visitor {

	private SingleItem item;
	
	public visitorMin (){
		item = new SingleItem("Champion", 10000000);
	}

	@Override
	public void visitSignleItem(SingleItem item) {
		if (this.item.getPrice() > item.getPrice()) {
			changeItem(item);
		}
	}

	@Override
	public void visitCompoundItem(CompoundItem item) {
		Iterator<Item> it = item.getItemsList().iterator();
		while (it.hasNext()) {
			it.next().accept(this);
		}
	}

	private void changeItem(SingleItem item) {
		this.item.setName(item.getName());
		this.item.setPrice(item.getPrice());
	}

	public SingleItem getItem() {
		return this.item;
	}

}
