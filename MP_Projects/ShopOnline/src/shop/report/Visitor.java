package shop.report;

import shop.item.CompoundItem;
import shop.item.SingleItem;

public interface Visitor {

	public void visitSignleItem(SingleItem item);

	public void visitCompoundItem(CompoundItem item);

}
